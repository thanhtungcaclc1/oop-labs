/** Lop CalculateBMI chua phuong thuc danh gia BMI
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 25 09
 * @version 1.0
 */
public class CalculateBMI {
	/** Ham calBMI danh gia chi so BMI
	 * @param w la can nang
	 * @param h la chieu cao
	 * @return Tu danh gia chi so BMI
	 */
	public String calBMI(double w, double h) {
		double bmi = w/(h*h);
		if (bmi<18.5) return "Thieu can";
		else if (bmi < 24.99) {
			if (bmi > 23) return "Thua can";
			else return "Binh thuong";
		}
		else return "Beo phi";
	}
}	
