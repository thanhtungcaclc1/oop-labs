/** Lop BiggerNumberTest kiem tra ket qua cua ham biggerNumber
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 25 09
 * @version 1.0
 */
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class BiggerNumberTest {
	BiggerNumber test = new BiggerNumber();
	// Cac ham test 
	@Test
	void test1() {
		int res1 = test.biggerNum(5, 6);
		assertEquals(6,res1);
	}
	@Test
	void test2() {
		int res2 = test.biggerNum(6, 7);
		assertEquals(7,res2);
	}
	@Test
	void test3() {
		int res3 = test.biggerNum(80, 0);
		assertEquals(80,res3);
	}
	@Test
	void test4() {	
		int res4 = test.biggerNum(-2, 7);
		assertEquals(7,res4);
	}
	@Test
	void test5() {
		int res5 = test.biggerNum(-5, -6);
		assertEquals(-5,res5);
	}
}

