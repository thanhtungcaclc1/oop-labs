/** Lop CalculateBMITest chua phuong thuc test kq cua ham calBMI
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 25 09
 * @version 1.0
 */
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculateBMITest {
	CalculateBMI test = new CalculateBMI();
	// Cac test kiem tra
	@Test
	void test1() {
		String res = test.calBMI(55, 1.7);
		assertEquals(res, "Binh thuong");
	}	
	@Test
	void test2() {	
		String res = test.calBMI(70, 1.7);
		assertEquals(res, "Thua can");
	}
	@Test
	void test3() {
		String res = test.calBMI(90, 1.7);
		assertEquals(res, "Beo phi");
	}
	@Test
	void test4() {
		String res = test.calBMI(40, 1.7);
		assertEquals(res, "Thieu can");
	}
	@Test
	void test5() {
		String res = test.calBMI(55, 1.7);
		assertEquals(res, "Binh thuong");
	}
}
