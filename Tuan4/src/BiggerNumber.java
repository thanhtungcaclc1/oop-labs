/** Lop BiggerNumber chua phuong thuc tim so lon hon trong 2 so
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 09 25
 * @version 1.0
 */
public class BiggerNumber {
	/** Ham biggerNum tim so lon nhat trong 2 so
	 * @param a,b a 2 so nhap vao
	 * @return so lon nhat trong 2 so a,b
	 */
	public int biggerNum(int a, int b) {
		if (a>=b) return a;
		else return b;
	}
}
