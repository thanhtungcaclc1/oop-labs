/* Lop MinOfArr chua phuong thuc tim so nho nhat trong mang
 * @author Nguye Ngoc Thanh Tung
 * @since 2018 09 25
 * @version 1.0
 */
public class MinOfArr {
	/**
	 * Ham minOfArr tim phan tu co gia tri nho nhat trong mang
	 * @param a la ten mang truyen vao
	 * @param n la so luong phan tu cua mang
	 * @return gia tri nho nhat cua mang
	 */
	public int minOfArr(int a[], int n) {
		int minNum=a[0];
		for (int i=0;i<n;i++) {
			if (a[i] < minNum)
				minNum = a[i];
		}
		return minNum;
	}
}
