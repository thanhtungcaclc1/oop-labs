/** Lop MinOfArrTest kiem tra ham minOfArr
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 25 09
 * @version 1.0
 */
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class MinOfArrTest {
	MinOfArr test = new MinOfArr();
	int n=5;
	// Cac ham test
	@Test
	void test1() {
		int a[]= {1,2,3,4,5};
		int res = test.minOfArr(a, n);
		assertEquals(res,1);
	}	
	@Test
	void test2() {
		int b[]= {-5,2,3,10,-10};
		int res = test.minOfArr(b, n);
		assertEquals(res, -10);
	}	
	@Test
	void test3() {
		int c[]= {100,2,3,-99,-10};
		int res = test.minOfArr(c, n);
		assertEquals(res, -99);
	}	
	@Test
	void test4() {
		int d[]= {100,200,300,10,-10};
		int res = test.minOfArr(d, n);
		assertEquals(res, -10);
	}	
	@Test
	void test5() {
		int e[]= {0,0,3,10,-5};
		int res = test.minOfArr(e, n);
		assertEquals(res, -5);
	}	
}

