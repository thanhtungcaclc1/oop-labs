/**
 * Class hoa qua quan li doi tuong hoa qua
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 09 30
 * @version 1.0
 */
class HoaQua {
	// khai bao cac thuoc tinh cua doi tuong hoa qua
	protected double giaBan;
	protected String xuatSu;
	protected String ngayNhap;
	protected int soLuong;
	
	// getter,setter
	public void setGiaBan(double gia) {
		this.giaBan=gia;
	}
	public void setXuatSu(String xs) {
		this.xuatSu=xs;
	}
	public void setNgayNhap(String dym) {
		this.ngayNhap=dym;
	}
	public void setSoLuong(int sl) {
		this.soLuong=sl;
	}
	
	public double getGiaban() {
		return this.giaBan;
	}
	public String getXuatSu() {
		return this.xuatSu;
	}
	public String getNgayNhap() {
		return this.ngayNhap;
	}
	public int getSoLuong() {
		return this.soLuong;
	}
	
	/** phuong thuc phan loai san pham hoa qua
	 * @return 1 trong 3 loai: re tien, trung binh, xin
	 */
	public String phanLoai() {	
		if (this.getGiaban() > 50000 && this.getGiaban() < 100000)
			return "Loai re tien";
		else if (this.getGiaban() >= 100000 && this.getGiaban() <= 200000)
			return "Loai trung binh";
		else return "Loai xin";
	}
	
	/**
	 * 
	 */
	public String kiemTraTonKho() {
		if (this.getSoLuong() > 100) return "Ton kho";
		else return "Con "+ this.getSoLuong() +" don vi trong kho";
	}
}
/** Lop QuaTao ke thua lop HoaQua quan li doi tuong qua tao
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 09 30
 * @version 1.0
 */
class QuaTao extends HoaQua {
	// Khai bao cac thuoc tinh cua qua tao 
	private String mauSac;
	private String loai;
	
	// getter, setter
	public String getMauSac() {
		return this.mauSac;
	}
	public String getLoai() {
		return this.loai;
	}

	/** phuong thuc tim muc dich su dung dua vao mau tao
	 * @return muc dich su dung
	 */
	public String mucDichSuDung() {
		if (this.getMauSac() == "Do") return "Trung bay";
		else if (this.getMauSac() == "Xanh") return "An cho dep da";
		else return "Khong an duoc";
	}
	
	public String timKhuVucXuatSu() {
		if (this.getXuatSu() == "Campuchia" || this.getXuatSu() == "Viet Nam")
			return "Dong Nam A";
		else if (this.getXuatSu() == "My" || this.getXuatSu() == "Anh")
			return "Chau Au";
		else return "Khong ro nguon goc xuat su";
	}
}
/** Lop QuaCam quan li doi tuong qua cam 
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 10 01
 * @version 1.0
 */
class QuaCam extends HoaQua{
	// khai bao thuoc tinh
	protected String loai;
	protected double canNang;
	
	// getter, setter
	public String getLoai() {
		return this.loai;
	}
	
	public void setLoai(String l) {
		this.loai = l;
	}
	
	public double getCanNang() {
		return this.canNang;
	}
	
	public void setCanNang(double cn) {
		this.canNang = cn;
	}
	
	/** phuong thuc danh gia can nang
	 * 	@return danh gia theo can nang
	 */
	public String danhGiaCanNang() {
		if (this.canNang > 100 && this.canNang < 200) return "Loai nho";
		else return "Loai to";
	}
}
/**Lop QuaCamSanh quan li doi tuong cam sanh
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 10 01
 * @version 1.0
 */
class QuaCamSanh extends QuaCam {
	/** phuong thuc tim sale 
	 * @return gia/1can
	 */
	public double timGiaSale() {
		if (this.loai == "ngon") return 100000;
		else if (this.loai == "trung binh" ) return 85000;
		else return 60000;
	}
}

/**Lop QuaCamCaoPhong quan li doi tuong cam cao phong
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 10 01
 * @version 1.0
 */
class QuaCamCaoPhong extends QuaCam {
	/** phuong thuc tim sale 
	 * @return gia/1can
	 */
	public double timGiaSale() {
		if (this.loai == "ngon") return 150000;
		else if (this.loai == "trung binh" ) return 100000;
		else return 80000;
	}
}


public class tuan5_bai1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HoaQua t = new HoaQua();
		t.setGiaBan(150000);
		System.out.println(t.phanLoai());
		t.setSoLuong(50);
		System.out.println(t.kiemTraTonKho());
		
		QuaTao t1 = new QuaTao();
		t1.setXuatSu("Campuchia");
		System.out.println(t1.timKhuVucXuatSu());
		
		QuaCamSanh t2 = new QuaCamSanh();
		t2.setLoai("to");
		System.out.println(t2.timGiaSale()+ " VND");
	
		}
}
