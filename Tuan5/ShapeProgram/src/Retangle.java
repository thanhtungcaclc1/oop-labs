/** Lop Retangle quan li doi tuong hinh chu nhat
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 10 01
 * @version 1.0
 */
class Retangle extends Shape{
	// thuoc tinh chieu dai chieu rong 
	private double width;
	private double length;
	
	// constructor
	public Retangle() {
		this.width = 1.0;
		this.length = 1.0;
	}
	public Retangle(double w, double l) {
		this.width = w;
		this.length = l;
	}
	public Retangle(double w, double l, String c, boolean f) {
		this.setColor(c);
		this.setFilled(f);
		this.length=l;
		this.width=w;
	}
	// getter,setter	
	public double getWidth() {
		return this.width;
	}
	public void setWidth(double w) {
		this.width=w;
	}
	public double getLength() {
		return this.length;
	}
	public void setLength(double l) {
		this.length=l;
	}
	
	/** ham tinh dien tich hcn
	 * @return dien tich hcn
	 */
	public double getArea() {
		return (this.getLength()*this.getWidth());
	}
	/** ham tinh chu vi hcn
	 *  @return chu vi hcn
	 */
	public double getPerimeter() {
		return (this.getLength()+this.getWidth())*2;
	}
	// toString method
	public String toString() {
		return super.toString();
	}
}