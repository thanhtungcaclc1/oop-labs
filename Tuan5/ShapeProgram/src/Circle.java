/** Lop Circle quan li doi tuong hinh tron
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 10 01
 * @version 1.0
 */
class Circle extends Shape{
	// khai bao thuoc tinh ban kinh cua doi tuong
	private double radius;
	
	// dinh nghia so PI
	final double PI = 3.14;
	
	// getter,setter
	public double getRadius() {
		return this.radius;
	}
	public void setRadius(double r) {
		this.radius = r;
	}

	// constructor
	public Circle() {
		this.setRadius(1.0);
	}
	public Circle(double r, String c, boolean f) {
		this.setRadius(r);
		this.setColor(c);
		this.setFilled(f);
	}
	
	/** phuong thuc tinh dien tich cua hinh tron
	 * @return dien tich hinh tron	
	 */
	public double getArea() {
		return PI*this.getRadius()*this.getRadius();	
	}
	/** phuong thuc tinh chu vi cua hinh tron
	 * @return chu vi hinh tron
	 */
	public double getPerimeter() {
		return 2*PI*this.getRadius();
	}
	
	// method toString
	public String toString() {
		return super.toString();
	}
}