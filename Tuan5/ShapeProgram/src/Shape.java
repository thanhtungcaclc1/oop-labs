/**Lop Shape quan li cac hinh dang trong hinh hoc
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 10 01
 * @version 1.0
 */
class Shape{
	// khai bao thuoc tinh mau va isFilled cua doi tuong
	private String color;
	private boolean filled;
	
	// getter, setter
	public String getColor() {
		return this.color;
	}
	public void setColor(String c) {
		this.color = c;
	}
	public boolean isFilled() {
		return this.filled;
	}
	public void setFilled(boolean b) {
		this.filled = b;
	}
	
	// constructor
	public Shape() {
		this.setColor("red");
		this.setFilled(true);
	}
	public Shape(String c, boolean b) {
		this.setColor(c);
		this.setFilled(b);
	}
	
	// toString method
	public String toString() {
		return this.getColor();
	}
}