/** lop Square quan li doi tuong hinh vuong
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 10 01
 * @version 1.0
 */
class Square extends Retangle{
	// constructor
	public Square() {
		this.setWidth(0.1);
		this.setLength(0.1);
	}
	public Square(double s) {
		this.setWidth(s);
		this.setLength(s);
	}
	public Square(double s, String c, boolean f) {
		this.setWidth(s);
		this.setLength(s);
		this.setColor(c);
		this.setFilled(f);
	}
	
	// getter,setter
	public double getSide() {
		return this.getWidth();
	}
	public void setSide(double s) {
		this.setWidth(s);
		this.setLength(s);
	}
	public double getWidth() {
		return super.getWidth();
	}
	public void setWidth(double w) {
		super.setWidth(w);
		super.setLength(w);
	}
	public double getLength() {
		return super.getLength();
	}
	public void setLength(double l) {
		super.setLength(l);
		super.setWidth(l);
	}
	
	// toString method
	public String toString() {
		return super.toString();
	}
	
}