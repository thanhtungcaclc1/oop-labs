/**
 * class tìm giá trị lớn nhất trong mảng
 */

import java.util.ArrayList;

public class FindMax
{

    public static <T extends Comparable> T max(ArrayList<T> arr)
    {
        T result = arr.get(0);
        for (int i=0;i<arr.size();i++)
        {
            if(result.compareTo(arr.get(i))<0)
                result = arr.get(i);
        }
        return result;
    }

    public static void main(String[] args)
    {
        ArrayList<Double> array = new ArrayList<Double>();
        array.add(1.5);
        array.add(8.1);
        array.add(4.9);
        array.add(7.2);
        array.add(10.5);
        System.out.println(max(array));

        ArrayList<Integer> arr1 = new ArrayList<Integer>();
        arr1.add(1);
        arr1.add(3);
        arr1.add(2);
        arr1.add(7);
        arr1.add(99);
        System.out.println(max(arr1));
    }
}
