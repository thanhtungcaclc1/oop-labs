public class BubbleSort
{
    public static <T> void print(T[] arr)
    {
        for(int i=0;i<arr.length;i++)
            System.out.print(arr[i] + " ");
        System.out.println();
    }


    public static <T extends Comparable> void sort(T[] arr)
    {
        for(int i = 0; i<arr.length; i++)
            for(int j =i+1; j <arr.length;j++)
                if(arr[i].compareTo(arr[j])>0)
                {
                    T tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
    }


    public static void main(String[] args)
    {
        Double[] arr2 = {8.9,0.2,5.6,9.1,4.5,9.0,3.2};
        sort(arr2);
        print(arr2);

        String[] arr1 = {"Thi", "Huy", "Tung", "Hoa", "Quang"};
        sort(arr1);
        print(arr1);

        Integer[] arr = {9,6,6,0,2,1,7};
        sort(arr);
        print(arr);


    }
}
