/** Lop AnalyzeUtils chua cac thao tac phuc vu viec tim ham trong file
 * @author: Nguyen Ngoc Thanh Tung
 * @since: 2018 10 10
 * @version: 1.0
 */
package Cau1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class AnalyzeUtils {
    /** Phuoong  thuc tim file theo ten trong folders
     * @param folderPath: duong dan cua folder
     * @param fileName: ten cua file can tim
     * @return file can tim
     */
    public static File findFileByName(String folderPath, String fileName){
        File folder = new File(folderPath);
        if (folder.isDirectory()) {
            File[] listFile = folder.listFiles();
            for (File eachFile : listFile) {
                if (eachFile.getName().equals(fileName)) return eachFile;
            }
        }
        return null;
    }

    /** Phuong thuc lay noi dung cua file
     * @param file: file can lay noi dung
     * @return 1 string la toan bo noi dung cua file
     */
    public static String getContent(File file){
        String res = "";
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                res += sc.nextLine();
                if (sc.hasNextLine()) res+="\n";
            }
        }catch (FileNotFoundException e){
            System.out.println("File not found");
        }
        return res;
    }

    /** Phuong thuc lay toan bo ham static trong file
     * @param path: file can lay cac ham static
     * @return 1 List gom cac ham static
     */
    public static List<String> getAllFunction(File path){
        List<String> staticFunctions = new ArrayList<>();
        String content = getContent(path);
        String[] lines = content.split("\n");
        Stack<String> checkCommented = new Stack<>();
        int openCommentPos=0,closeCommentPos=0;
        for (int i=0;i<lines.length;i++){
            lines[i]=lines[i].trim();
            String stardard = "";
            int o=0;
//            while (o<lines.length){
//                if (lines[i].charAt(o)!='/') stardard+=lines[i].charAt(o);
//                o++;
//            }
            if (lines[i].indexOf("/*")!=-1 && lines[i].indexOf("/**")==-1){
                openCommentPos=i;
                checkCommented.push("/*");
                for (int u=i+1;u<lines.length;u++){
                    if (lines[u].indexOf("/*")!=-1) checkCommented.push("/*");
                    if (lines[u].indexOf("*/")!=-1) checkCommented.pop();
                    if (checkCommented.isEmpty()==true){
                        closeCommentPos=u;
                        break;
                    }
                }
            }
            if (lines[i].substring(0,lines[i].indexOf("{")+1).indexOf("public static")!=-1 && lines[i].indexOf(";")==-1){
                int startPos=i,endPos=0;
                Stack<String> braces = new Stack<>();
                if (lines[i].indexOf("{")!=-1) {
                    braces.push("{");
                }
                for (int j = i+1; j < lines.length; j++) {
                    if (lines[j].indexOf("{") != -1) braces.push("{");
                    if (lines[j].indexOf("}") != -1) braces.pop();
                    if (braces.isEmpty()) {
                        endPos = j;
                        break;
                    }
                }
                String line ="";
//                System.out.println("-" + openCommentPos + lines[openCommentPos] + startPos +"\n");
//
//                System.out.println(" " +closeCommentPos + lines[closeCommentPos] + endPos +"\n") ;

                for (int k = startPos; k <= endPos; k++) {
                    line += lines[k] + "\n";
                }

                boolean check=true;
                if (startPos>=openCommentPos && endPos<=closeCommentPos){
                    check=false;
                }
                if (check==true)
                    staticFunctions.add(line);
            }
        }
        return staticFunctions;
    }

    /** Phuong thuc tim ham theo ten
     * @param path: file chua ham can tim
     * @param name: ten ham can tim
     * @return noi dung ham can tim hoac in ra Not found neu k tim thay
     */
    public static String findFunctionByName(File path, String name) {
        List<String> functions = new ArrayList<>();
        functions = getAllFunction(path);

        for (int i=0;i<functions.size();i++){
            String temp = functions.get(i);
            if (temp.indexOf("//")==-1 && temp.indexOf("/*")==-1) {
                String[] lines = temp.split("\n");
                String functionName="";
                if (lines[0].indexOf("{")!=-1)
                    functionName = lines[0].replace("{", "");
                else functionName = lines[0].trim();
                if (functionName.trim().substring(0, functionName.trim().indexOf("(")).equals(name.substring(0, name.indexOf("(")))) {
                    String param = functionName.substring(functionName.indexOf("(") + 1, functionName.indexOf(")"));
                    String[] params = param.split(",");
                    String paramOfName = name.substring(name.indexOf("(") + 1, name.indexOf(")"));
                    String[] paramsName = paramOfName.split(",");
                    for (int j = 0; j < paramsName.length; j++) {
                        String part = params[j];
                        if (part.indexOf(paramsName[j]) != -1)
                            return temp;
                    }
                }
            }
        }
        return "Not found";
    }
}
