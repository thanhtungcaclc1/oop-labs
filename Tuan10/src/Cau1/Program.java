/** Lop Program test thu chuong trinh
 * @author Nguyen Ngoc Thanh Tung
 * @version 1.0
 * @since 2018 10 10
 */
package Cau1;

import java.io.File;
import java.util.List;

import static Cau1.AnalyzeUtils.findFileByName;
import static Cau1.AnalyzeUtils.getContent;
import static Cau1.AnalyzeUtils.getAllFunction;
import static Cau1.AnalyzeUtils.findFunctionByName;

public class Program {
    public static void main(String[] args) {
        File newFile = findFileByName("C:\\Users\\clone\\Desktop\\Tuan9\\src","Utils.java");
        List<String> functions = getAllFunction(newFile);
        for (int i=0;i<functions.size();i++){
            System.out.println("Function "+ i + ": \n" + functions.get(i));
        }
        System.out.println("Function found: \n"+findFunctionByName(newFile,"public static File findFileByName(String,String)"));
    }
}
