/** Lop sap xep mang gom cac phan tu duoc sinh ngau nhien
 * @author Nguyen Ngoc Thanh Tung
 * @version 1.0
 * @since 2018 10 10
 */
package Cau2;

import java.util.*;

public class BubbleSort {
    /**
     * khai bao thuoc tinh mang numbers
     */
    private ArrayList<Double> numbers = new ArrayList<Double>();

    /**
     * sinh cac so ngau nhien va gan gia tri cho cac phan tu
     */
    public void generate(){
        for (int i=0;i<1000;i++){
            double x = (Math.random()*(1000-1)+1);
            numbers.add(x);
        }
    }

    /**
     * sap xep cac phan tu cua mang
     */
    public void sort(){
        int n = numbers.size();
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (numbers.get(j) > numbers.get(j+1)) {
//                    double tmp = numbers.get(j);
//                    numbers.set(j, numbers.get(j+1));
//                    numbers.set(j+1, tmp);
                     numbers.set(j,numbers.get(j+1)+numbers.get(j));
                     numbers.set(j+1,numbers.get(j)-numbers.get(j+1));
                     numbers.set(j,numbers.get(j)-numbers.get(j+1));
                }
    }

    /**
     * in ra cac phan tu cua mang
     */
    public void print(){
        for (int i=0;i<numbers.size();i++){
            System.out.println(numbers.get(i));
        }
    }
}
