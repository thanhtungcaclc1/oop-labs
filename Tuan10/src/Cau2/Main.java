/** Lop chay thu cac phuong thuc
 * @version 1.0
 * @since 2018 10 10
 * @author Nguyen Ngoc Thanh Tung
 */
package Cau2;

public class Main {
    public static void main(String[] args) {
        BubbleSort bubbleSort = new BubbleSort();
        bubbleSort.generate();
        bubbleSort.sort();
        bubbleSort.print();
    }

}
