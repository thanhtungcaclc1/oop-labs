/**
 * Lop Student bieu dien cac thong tin cua 1 sinh vien
 * @param name la ten sinh vien
 * @param id la ma so sinh vien 
 * @param group la nhom cua sinh vien
 * @param email la email cua sinh vien
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 09 08
 * @version 10.0.2	
 */
class Student {
	// Khai bao cac thuoc tinh cua sinh vien
	private String name;
	private String id;
	private String group;
	private String email;
	
	// Phuong thuc khoi tao khong co tham so
	public Student() {
		this.name = "Student";
		this.id = "000";
		this.group = "K59CB";
		this.email = "uet@vnu.edu.vn";
	}
	
	// Phuong thuc khoi tao co tham so
	public Student(String n, String sid, String em) {
		this.name = n;
		this.id = sid;
		this.email = em;
		this.group = "K59CB";
	}
	
	//Phuong thuc khoi tao sao chep
	public Student(Student s) {
		this.name = s.getName();
		this.id = s.getId();
		this.group = s.getGroup();
		this.email = s.getEmail();
	}
	
	// Phuong thuc tra ve ten sinh vien
	public String getName() {
		return this.name;
	}
	
	// Phuong thuc khoi tao gia tri cua thuoc tinh ten sinh vien
	public void setName(String str) {
		this.name = str;
	}
	
	// Phuong thuc tra ve ma so sinh vien
	public String getId() {
		return this.id;
	}
	
	//Phuong thuc khoi tao gia tri thuoc tinh ma so sinh vien
	public void setId(String str) {
		this.id = str;
	}
	
	// Phuong thuc tra ve nhom cua sinh vien
	public String getGroup() {
		return this.group;
	}
	
	// Phuong thuc khoi tao gia tri thuoc tinh nhom 
	public void setGroup(String str) {
		this.group = str;
	}
	
	// Phuong thuc tra ve email cua sinh vien
	public String getEmail() {
		return this.email;
	}
	
	// Phuong thuc khoi tao gia tri thuoc tinh email
	public void setEmail(String str) {
		this.email = str;
	}
	
	// Phuong thuc tra ve thong tin ten, mssv, nhom, email cua sinh vien
	String getInfo() {
		String info = name + " " + id + " " + group + " " + email;
		return info;
	}
}

/**
 * Lop StudentManagement cung cap mot so thao tac quan li  1 danh sach sinh vien 
 * @param students la mang sinh vien bieu dien 1 danh sach sinh vien co 100 sinh vien
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 09 09
 * @version 10.0.2
 */
public class StudentManagement {
	// Thuoc tinh students la array chua cac doi tuong thuoc lop Student
	Student[] students = new Student[100];
	
	// Phuong thuc khoi tao gia tri cho danh sach sinh vien
	public void initiateValue() {
		for (int i=0;i<100;i++)
			students[i] = new Student();
	}
	
	// Phuong thuc in ra ten sinh vien theo tung nhom
	public void studentsByGroup() {
		
		// mang check kiem tra xem sinh vien thu i da duoc chon hay chua
		boolean[] check = new boolean[100];
		for (int i=0;i<100;i++)
			check[i]=true;
		// xep sinh vien theo tung nhom va in ra man hinh
		for (int i=0;i<100;i++) {
			if (check[i]==true) {
				System.out.print(students[i].getGroup() + ": " + students[i].getName() + " ");
				for (int j=i+1;j<100;j++) {
					if (students[i].getGroup() == students[j].getGroup()) {
						System.out.print(students[j].getName() + " ");
						check[j]=false;
					}
				}
				System.out.println(" ");
			}
		}
	}
	
	// Phuong thuc xoa 1 sinh vien co mssv la id ra khoi danh sach
	public void removeStudent(String id) {
		
		// Tim vi tri cua sinh vien co ma so id trong danh sach
		int index=0; 
		for (int i=0;i<100;i++) { 
			if (students[i].getId() == id) {
				 index = i;
				 break;
			}
		}
		
		// Xoa sinh vien ra khoi danh sach
		for (int i=index;i<99;i++) {
			 students[i] = students[i+1] ;
		}
		 
		// In lai danh sach sinh vien sasu khi xoa
		System.out.println("Removed the student having id " + id + ": ");
		for (int i=0;i<99;i++) {
			 System.out.print(students[i].getName() + " ") ;
		}
	}
	
	// Phuong thuc kiem tra 2 sinh vien co thuoc cung 1 group khong
	public boolean sameGroup(Student s1, Student s2) {
		boolean res=false;
		String gr1 = s1.getGroup();
		String gr2 = s2.getGroup();
		if (gr1 == gr2) res=true;
		else res=false;
		return res;
	}
	
	public static void main(String[] args) {
		// Tao doi tuong test thuoc class StudenManagement de su dung cac phuong thuc cua class nay
		StudentManagement test = new StudentManagement();
		
		// Khoi tao gia tri cho toan bo danh sach sinh vien
		test.initiateValue();
		
		test.students[0] = new Student("Tung", "17021353", "cloneoftung");
		test.students[0].setGroup("K62CACLC1");
		
		test.students[1] = new Student("Tuan", "17021350", "cloneoftuan");
		test.students[1].setGroup("K62CACLC1");
		
		test.students[2] = new Student("Huy", "17021263", "cloneoftung");
		test.students[2].setGroup("K62CACLC2");
		
		// Khai bao doi tuong s1 va khoi tao gia tri 
		Student s1 = new Student();
		
		// Gan gia tri cho s1 dung phuong thuc set
		s1.setName("Nguyen Ngoc Thanh Tung");
		s1.setId("17021353");
		s1.setGroup("N2"); 
		s1.setEmail("cloneoftung@gmail.com");
		
		// In thong ten va in thong tin cua s1 ra man hinh
		System.out.println(s1.getName());
		System.out.println(s1.getInfo());
		
		// Khai bao doi tuong s2, khoi tao gia tri va in thong tin ra man hinh 
		Student s2 = new Student();
		System.out.println(s2.getInfo());
		
		// Khai bao doi tuong s3, khoi tao gia tri dung ham co tham so, in thong tin ra man hinh 
		Student s3 = new Student("Tung Nguyen", "17022508", "tung@gmail.com");
		System.out.println(s3.getInfo());
		
		// Khai bao doi tuong s4 voi cac thuoc tinh co gia tri duoc copy tuong ung tu s1
		Student s4 = new Student(s1);
		System.out.println(s4.getInfo());
		
		// Thay doi gia tri thuoc tinh nhom cua s1,s2,s3
		s3.setGroup("K59CLC");
		s1.setGroup("K59CLC");
		s2.setGroup("K59CB");
		
		// Kiem tra s1,s2,s3 co cung nhom khong
		System.out.println(test.sameGroup(s1, s3));
		System.out.println(test.sameGroup(s1, s2));
		
		// Xep cac sinh vien theo tung nhom
		test.studentsByGroup();
		
		// Xoa sinh vien ra khoi danh sach
		test.removeStudent("17021353");
	}
}
	