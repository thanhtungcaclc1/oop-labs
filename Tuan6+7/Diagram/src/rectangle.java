/** lop quan li cac doi tuong hinh chu nhat
 * @author Nguyen Ngoc Thanh Tung
 * @version 1.0
 * @since 2018 10 09
 */
public class rectangle extends shape {	
	// thuoc tinh chieu dai chieu rong
	private double a,b;
	
	// constructor
	public rectangle() {
		super("rectangle");
	}
	
	public rectangle(String mau, double x, double y) {
		super("rectangle",mau,x,y);
	}
	
	public rectangle(String mau, double x, double y, double a, double b) {
		super("rectangle", mau, x, y);
		this.a=a;
		this.b=b;
	}
	
	// getter, setter
	public void setChieuDai(double a) {
		this.a=a;
	}
	public void setChieuRong(double b) {
		this.b=b;
	}
	public double getChieuDai() {
		return this.a;
	}
	public double getChieuRong() {
		return this.b;
	}
}
