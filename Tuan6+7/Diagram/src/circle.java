/** lop quan li cac doi tuong hinh tron
 * @author Nguyen Ngoc Thanh Tung
 * @version 1.0
 * @since 2018 10 09
 */
public class circle extends shape {
	// thuoc tinh ban kinh
	private double r;
	
	// constructor	
	public circle() {
		super("circle");
	}
	
	public circle(String mau, double x, double y) {
		super("circle", mau, x, y);
	}
	
	public circle(String mau, double x, double y, double r) {
		super("circle", mau, x, y);
		this.r=r;
	}
	
	// getter,setter
	public void setBanKinh(double r) {
		this.r=r;
	}
	public double getBanKinh() {
		return this.r;
	}
	
	// hien thi
	public void print() {
		super.print();
		System.out.println(" "+this.getBanKinh());
	}
	
	// compare
	//public boolean compareCircle(shape c1, shape c2) {
	//	if (c1.getType() == "circle" && c2.getType()=="circle") {
	//		if  (c1.getX() == c2.getX() && c1.getY()==c2.getY() && c1.getBanKinh() == c2.getBanKinh()) {
	//			return true;
	//		}
	//	}
	//	else return false;
	//}
	
	// check 2 circle
	public boolean isDuplicatedCircle(circle c1) {
		if (this.getX() == c1.getX() && this.getY() == c1.getY() && this.getBanKinh() == c1.getBanKinh()){
			return true;
		}
		else return false;
	}
}
