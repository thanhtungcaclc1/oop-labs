/** lop shape dai dien cho cac hinh ve khac nhau
 * @author Nguyen Ngoc Thanh Tung
 * @version 1.0
 * @since 2018 10 09
 */
public class shape{
	// khai bao cac thuoc tinh
	private String loaiHinh;
	private String mauSac;
	private double x,y;
	
	// constructor
	public shape(String loai) {
		this.x=0;
		this.y=0;
		this.loaiHinh=loai;
		this.mauSac="red";
	}
	public shape(String loai, String mau, double tungdo, double hoanhdo) {
		this.loaiHinh=loai;
		this.mauSac=mau;
		this.x=tungdo;
		this.y=hoanhdo;
	}
	
	// getter,setter
	public void setColor(String Color) {
        this.mauSac = Color;
    }

    public void setType(String Type) {
        this.loaiHinh = Type;
    }

    public void setX(double X) {
        this.x = X;
    }

    public void setY(double Y) {
        this.y = Y;
    }

    public String getType() {
        return this.loaiHinh;
    }

    public String getColor() {
        return this.mauSac;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }
    
    public void diChuyen(double xx, double yy) {
    	this.setX(xx);
    	this.setY(yy);
    } 
    
    // hien thi
    public void print() {
    	System.out.println(this.getType() +" " + this.getColor() +" " +this.getX() +" "+ this.getY());
    }
    
    public String toString() {
    	String s = this.getType() + this.getColor() + this.getX() + this.getY();
    	return s;
    }
    
}