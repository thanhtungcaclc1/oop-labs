/** Class program kiem tra cac phuong thuc
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 05 10
 * @version 1.0
 */
public class Program {

	public static void main(String[] args) {
		diagram d = new diagram();
		
		circle c1 = new circle("white",1,2,3);
		square sq1 = new square("black",2,3,4);
		rectangle rt1 = new rectangle("red",3, 4,6,9); 
		triangle tri1 = new triangle("pink",4,5, 1,2,3);
		
		layer l1 = new layer();
		l1.addShape(sq1);
		l1.addShape(tri1);
		l1.addShape(rt1);
		l1.addShape(c1);
		
		d.addLayer(l1);
		
	}

}
