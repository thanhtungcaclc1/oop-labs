import java.util.*;
/** lop layer dai dien cho lop ve
 * @author Nguyen Ngoc Thanh Tung
 * @version 1.0
 * @since 2018 10 09
 */
public class layer {
	// doi tuong layer chua cac doi tuong shape
	private ArrayList<shape> shapeList=new ArrayList<>();
	private boolean isVisible;
	
	// getter,setter
	public void setVisible(boolean v) {
		this.isVisible = v;
	}
	public boolean getVisible() {
		return this.isVisible;
	}
	
	/** phuong thuc xoa hinh tam giac
	 */
	public void delelteTriangle() {
		for (int i=0;i<shapeList.size();i++) {
			if (shapeList.get(i).getType().equals("triangle"))
				shapeList.remove(i);
		}
	}
	
	/** phuong thuc xoa hinh tron 
	 */
	public void deleteCircle() {
		for (int i=0;i<shapeList.size();i++) {
			if (shapeList.get(i).getType().equals("circle"))
				shapeList.remove(i);
		}
	}
	
	/** phuong thuc xoa hinh chu nhat
	 */
	public void deleteRectangle() {
		for (int i=0;i<shapeList.size();i++) {
			if (shapeList.get(i).getType().equals("rectangle"))
				shapeList.remove(i);
		}
	}
	
	/** phuong thuc them doi tuong vao danh sach cac shape
	 */
	public void addShape(shape s) {
		shapeList.add(s);
	}
	
	// hien thi
	public void print() {
		for (int i=0;i<shapeList.size();i++)
			System.out.println(shapeList.get(i).getType());
	}
	
	public String toString() {
		String s = "";
		for (int i=0;i<shapeList.size();i++) {
			s+=shapeList.get(i).toString();
		}
		return s;
	}

}
