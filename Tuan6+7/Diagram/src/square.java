/** lop dai dien cho cac doi tuong hinh vuong 
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 10 09
 * @version 1.0
 */
public class square extends shape{
	// thuoc tinh canh
	private double a;
	
	// constructor
	public square() {
		super("square");
	}
	
	public square(String mau, double x, double y) {
		super("square",mau,x, y);
	}
	
	public square(String mau, double x, double y, double aa) {
		super("square",mau,x, y);
		this.a=aa;
	}
	
	// getter, setter
	public void setCanh(double a) {
		this.a=a;
	}
	public double getCanh() {
		return this.a;
	}
	
	// hien thi
	public void print() {
		super.print();
		System.out.println(" " + this.getCanh()) ;
	}
}
