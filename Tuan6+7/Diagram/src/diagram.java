import java.util.*;
/** lop diagram quan li bieu dien bieu do dang duoc ve
 * @author Nguyen Ngoc Thanh Tung
 * @version 1.0
 * @since 2018 10 09
 */
public class diagram {
	// lop diagram chua cac doi tuong layer
	private ArrayList<layer> layerList = new ArrayList<>();
	
	/* phuong thuc xoa hinh tron
	 */
	public void deleteCircle() {
		for (int i=0;i<layerList.size();i++) {
			layerList.get(i).deleteCircle();
		}
	}
	
	// hien thi
	public void print() {
		for (int i=0;i<layerList.size();i++) {
			System.out.println("layer " + i);
			layerList.get(i).print();
		}
	}
	
	// them doi tuong layer
	public void addLayer(layer l) {
		layerList.add(l);
	}
	
	
}
