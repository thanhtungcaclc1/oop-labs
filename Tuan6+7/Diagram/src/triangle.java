/** lop triangle dai dien cho cac doi tuong hinh tam giac
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 10 09
 * @version 1.0
 */
public class triangle extends shape {
	// cac thuoc tinh canh cua hinh tam giac
	private double a,b,c;
	
	// constructor
	public triangle() {
		super("triangle");
	}
	
	public triangle(String mau, double x, double y) {
		super("triangle", mau, x, y);
	}
	
	public triangle(String mau, double x, double y, double aa, double bb, double cc) {
		super("triangle" , mau, x, y);
		this.a=aa;
		this.b=bb;
		this.c=cc;
	}
	// getter, setter
	public double getA() {
		return this.a;
	}
	public double getB() {
		return this.b;
	}
	public double getC() {
		return this.c;
	}
	public void setA(double aa) {
		this.a=aa;
	}
	public void setB(double bb) {
		this.b=bb;
	}
	public void setC(double cc) {
		this.c=cc;
	}

	// hien thi
	public void print() {
		super.print();
		System.out.println(" "+this.getA() + " " + this.getB() + " "+this.getC());
	}
}
