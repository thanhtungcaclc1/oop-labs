/**
 * Lop phanso bieu dien cac thao tac voi phan so
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 09 18
 * @version 10.0.2
 */
public class phanso {
	// Khai bao thuoc tinh tu va mau cua phan so
	private int tu;
	private int mau;
	
	/*
	 * Getter/Setter
	 */
	public void setTu(int n) {
		this.tu = n;
	}
	
	public int getTu() {
		return this.tu;
	}
	
	public void setMau(int n) {
		this.mau = n;
	}
		
	public int getMau() {
		return this.mau;
	}
	
	/** ham khoi tao phanso() khong co tham so
	 * @return phan so voi gia tri cua tu va mau la 1
	 */
	public phanso() {
		this.tu=1;
		this.mau=1;
	}
	
	/**Ham khoi tao co tham so
	 * @param t,m la gia tri cua tu so va mau so de khoi tao cho phan so
	 * @return phan so voi gia tri tu = t va mau = m
	 */
	public phanso(int t, int m) {
		this.tu=t;
		this.mau=m;
	}
	
	/*
	 * ham toString
	 */
	public String toString() {
		return this.getTu() + "/" + this.getMau();
	}
	
	/**
	 * ham findGCD tim uoc chung lon nhat
	 * @param a,b la 2 so nhap vao
	 * @return uoc chung cua 2 so a,b 
	 */
	public static int findGCD(int t, int m ) {
		int res = 0;
		if (t<0) t=-t;
		if (m<0) m=-m;
		while (t!=m) {
			if (t>m) t-=m;
			else m-=t;
		}
		res=t;
		return res;
	}
	
	/** Tim boi chung nho nhat
	 * @param a,b la 2 so nhap vao
	 * @return bcnn cua 2 so a,b
	 */
	public static int findLCM(int t, int m) {
		return (t*m)/findGCD(t,m);
	}
	
	/** ham printPs in phan so ra ngoai man hinh
	 * @param phan so s
	 * @return in phan so ra ngoai man hinh
	 */
	public static phanso printPs(phanso s) {
		int uocChung=1;
		if (findGCD(s.getTu(), s.getMau()) > 0) {
			uocChung = findGCD(s.getTu(), s.getMau());
		}
		//System.out.println(s.getTu()/uocChung + "/" + s.getMau()/uocChung);
		
		return new phanso(s.getTu()/uocChung,s.getMau()/uocChung);
	}
	
	/** ham congPS thuc hien cong 2 phan so
	 * @param ps1,ps2 la 2 phan so
	 * @return tong cua 2 phan so ps1,ps2
	 */
	public phanso congPS(phanso ps1, phanso ps2) {
		int mauChung = findLCM(ps1.getMau(), ps2.getMau());
		phanso kq = new phanso(1,2);	
		kq.setTu(ps1.tu*(mauChung/ps1.mau) + ps2.tu*(mauChung/ps2.mau));
		kq.setMau(mauChung);
		return printPs(kq);
	}
	

	/** ham truPS thuc hien tru 2 phan so
	 * @param ps1,ps2 la 2 phan so
	 * @return hieu cua 2 phan so ps1,ps2
	 */
	public phanso truPS(phanso ps1, phanso ps2) {
		int mauChung = findLCM(ps1.getMau(), ps2.getMau());
		phanso kq = new phanso(1,2);	
		kq.setTu(ps1.tu*(mauChung/ps1.mau) - ps2.tu*(mauChung/ps2.mau));
		kq.setMau(mauChung);
		return printPs(kq);
	}
	

	/** ham nhanPS thuc hien nhan 2 phan so
	 * @param ps1,ps2 la 2 phan so
	 * @return tich cua 2 phan ps1,ps2
	 */
	public phanso nhanPs(phanso ps1, phanso ps2) {
		phanso kq = new phanso();
		kq.setTu(ps1.getTu()*ps2.getTu());
		kq.setMau(ps1.getMau()*ps2.getMau());
		return printPs(kq);
	}
	

	/** ham chiaPS thuc hien chia 2 phan so
	 * @param ps1,ps2 la 2 phan so
	 * @return thuong cua 2 phan so ps1,ps2
	 */
	public phanso chiaPs(phanso ps1, phanso ps2) {
		phanso kq = new phanso();
		kq.setTu(ps1.getTu()*ps2.getMau());
		kq.setMau(ps1.getMau()*ps2.getTu());
		return printPs(kq);
	}
	

	/** ham equals thuc hien so sanh 2 phan so
	 * @param ps1,ps2 la 2 phan so
	 * @return neu 2 phan so bang nhau thi tra ve true, khong thi nguoc lai
	 */
	public boolean equals(Object obj) {
		phanso ps=(phanso) obj;
		return (ps.getTu()*this.mau == ps.getMau()*this.tu);
	}
	

	/** ham main thuc hien cac thao tac voi cac phan so
	 * @param ps1,ps2 la 2 phan so
	 * @param test la doi tuong thuoc class de goi ra thuc hien cac ham
	 * @return ket qua cua phep cong tru nhan chia va so sanh 2 phan so
	 */
	public static void main(String[] args) {
		
		// khai bao cac doi tuong thuoc lop phanso
		phanso test = new phanso();
		phanso ps1 = new phanso(1,2);
		phanso ps2 = new phanso(4,8);
		
		// phuong thuc cong phan so
		System.out.println(test.congPS(ps1, ps2));
		
		/*
		// phuong thuc tru phan so
		System.out.println(test.truPS(ps1, ps2));
		
		// phuong thuc nhan 2 phan so
		System.out.println(test.nhanPs(ps1, ps2));
		
		// Chia 2 phan so 
		System.out.println(test.chiaPs(ps1, ps2));
		
		// Kiem tra 2 phan so co bang nhau
		System.out.println(ps1.equals(ps2));
		*/
		
	}
}
