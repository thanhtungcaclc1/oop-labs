/**
 * Lop doMath bieu dien cac thao tac tim ucln va tim so fibonaci thu n
 * @author Nguyen Ngoc Thanh Tung
 * @since 2018 10 09
 * @version 10.0.2
 */
public class doMath {
	
	/** Tim uoc so chung lon nhat cua a va b
	 * @param a,b la cac so nhap vao de im ucln
	 * @return tra ve ucln cua a,b
	 */
	public int findGCD(int a, int b) {
		int res=0;
		if (a<0) a=-a;
		if (b<0) b=-b;
		while (a!=b) {
			if (a>b) a-=b;
			else b-=a;
		}
		res=a;
		return res;
	}
	
	/** Tim so fibonaci thu n 
	 * @param n la vi tri cua so fiboanci can tim
	 * @return so fibonaci o vi tri thu n
	 */
	public int findFibo(int n) {
		int x1,x2,x3;
		if (n == 1) return 1;
		else if (n <= 0) return 0;
		else {
			x1=0;
			x2=1;
			x3=x1+x2;
			for (int i=3;i<=n;i++) {
				x1=x2;
				x2=x3;
				x3=x1+x2;
			}
		}
		return x3;
	}
	
	/** ham main thuc hien cac phep toan tim ucln va tim so fibo thu n
	 * @param test la doi tuong thuoc class de kiem tra cac method
	 * @return ket qua cua phep tim ucln va tim so fibo thu n
	 */
	public static void main(String[] args) {
		// Khai bao doi tuong test thuoc class de kiem tra method
		doMath test = new doMath();
		
		// Chay thu ham tim UCLN va tim so fibo thu n
		System.out.println(test.findGCD(5,15));
		System.out.println(test.findFibo(2));
	}

}
