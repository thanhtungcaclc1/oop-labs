/** Class Utils bao gom cac thao tac voi file
 * @author Nguyen Ngoc Thanh Tung
 * @since 10 10 2018
 * @version 1.0
 */
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;
import java.util.Scanner;

public class Utils {
    public static String a;

    /** Doc du lieu tu file
     * @param path: duong dan toi file
     * @return noi dung trong file hoac la thong bao: file khong ton tai
     */
   
   public static /**/ String readContentFromFile(String path){ 
        String res = "";
        try {
            FileInputStream file = new FileInputStream(path);
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                res += sc.nextLine();
                if  (sc.hasNextLine()) res+="\n";
            }
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist");
        }
        return res;
    }
    

    /** Ghi du lieu ra file
     * @param path: du lieu den file, 1 de ghi de, 2 de them noi dung vao
     */
    public static void writeContentToFile(String path){
        Scanner sc = new Scanner(System.in);
        System.out.print("Write content to file: ");
        String addedContent = sc.nextLine() + "\n";
        byte[] strToByte = addedContent.getBytes();
        System.out.print("Enter 1 for overwrite content, 2 for append content: ");
        int op = sc.nextInt();
        if (op == 1 ) {
            try {
                FileOutputStream file = new FileOutputStream(path, false);
                try {
                    file.write(strToByte);
                    System.out.println("Added to file");

                } catch (IOException e) {
                    System.out.println("Loi vao ra");
                }
            } catch (FileNotFoundException e) {
                System.out.println("File not found");
            }
        }
        else if (op==2){
            try{
                FileOutputStream file = new FileOutputStream(path,true);
                try{
                    file.write(strToByte);
                    System.out.println("Appended to file");
                }catch (IOException e){
                    System.out.println("Loi vao ra");
                }
            }catch (FileNotFoundException e){
                System.out.println("File not found");
            }
        }else{
            System.out.println("sai option");
        }
    }

    /** Tim file trong folder
     * @param folderPath: duong dan cua folder
     * @param fileName: duong dan cua file
     * @return file trong folder neu ton tai, hoac la null
     */
    public static File findFileByName(String folderPath, String fileName){
        File folder = new File(folderPath);
        if (folder.isDirectory()) {
            File[] listFile = folder.listFiles();
            for (File eachFile : listFile) {
                if (eachFile.getName().equals(fileName)) return eachFile;
            }

        }
        return null;
    }

    /** Doc noi dung trong file
     * @param file: duong dan cua file
     * @return noi dung trong file
     */
    
    public static String readContent(File file){ // static 
        String res = "";
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                res += sc.nextLine();
                if (sc.hasNextLine()) res+="\n";
            }
        }catch (FileNotFoundException e){
            System.out.println("File not found");
        }
        return res;
    }

    public static void main(String[] args){
        String stringFromFile = readContentFromFile("C:\\Users\\clone\\Desktop\\Tuan9\\src//FileIn.txt");
        System.out.println(stringFromFile);

        writeContentToFile("C:\\Users\\clone\\Desktop\\Tuan9\\src//FileOut.txt");

        File newFile = findFileByName("C:\\Users\\clone\\Desktop\\Tuan9\\src","FileIn.txt");
        if (newFile==null) System.out.println("File not found");
        else System.out.println(readContent(newFile));
    }
}